import com.ignitesales.sbt.plugin.guide.IgniteGuidePluginKeys
import com.typesafe.sbt.S3Keys
import com.typesafe.sbt.osgi.OsgiKeys

lazy val igniteEnvironment = project
  .settings(
    igniteGuideEnvironment := "development"
  )

val dataserviceVersion = "5.16.0"
val bundlePath = "ignite-rgt"

lazy val commonSettings =
  Seq(
    organization := "com.ignitesales",
    libraryDependencies ++= Seq("com.ignitesales" %% "dataservice" % dataserviceVersion),
    igniteGuideEnvironment := (igniteGuideEnvironment in igniteEnvironment).value,
    IgniteGuidePluginKeys.igniteBundlePath := bundlePath,
    OsgiKeys.importPackage := Seq("javax.activation;version=1.1", "*")
  )

lazy val root = (project in file("."))
  .aggregate(
    templateGuide
  )
  .enablePlugins(IgniteGuidePlugin)
  .settings(
    name := "ignite-rgt",
    mappings in S3Keys.s3Upload := Nil,
    aggregate in S3Keys.s3Delete := false,
    aggregate in IgniteGuidePluginKeys.igniteBundleList := false,
    IgniteGuidePluginKeys.igniteBundlePath := bundlePath,
    IgniteGuidePluginKeys.igniteGuideEnvironment := (IgniteGuidePluginKeys.igniteGuideEnvironment in igniteEnvironment).value
  )

lazy val templateGuide = (project in file("guide"))
  .enablePlugins(IgniteGuidePlugin)
  .enablePlugins(IgniteNodeBuildPlugin)
  .enablePlugins(NpmPlugin)
  .enablePlugins(JsBuild)
  .settings({
    commonSettings ++
      Seq(
        name := "ignite-rgt-guide",
        libraryDependencies ++= Seq(
          "org.webjars.npm" % "jquery" % "3.2.1",
          "org.webjars.npm" % "font-awesome" % "4.7.0",
          "org.webjars.npm" % "bootstrap" % "4.1.3",
          "org.webjars.npm" % "tether" % "1.4.0",
          "org.webjars" % "ionicons" % "2.0.1"
        )
      )
  })
