
const configureMockQuestionsToSaveLocal = questionGroups => {
  return questionGroups.map(questionGroup => {
    questionGroup.questions = (questionGroup.questions || []).map(question => {
      return {
        ...question,
        attributes: {
          ...(question.attributes || {}),
          saveLocalOnly: true
        }
      }
    });

    return questionGroup;
  });
};


export default configureMockQuestionsToSaveLocal;
