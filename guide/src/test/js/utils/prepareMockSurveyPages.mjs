import configureMockQuestionsToSaveLocal from "./configureMockQuestionsToSaveLocal";
//TODO: move this to a mock-survey-page npm pkg
const prepareMockSurveyPages = (pages, pathName) => pages.map(page => {
  page.pathKey = pathName;
  page.pathPage = page.key;
  page.page = {
    questionGroups: configureMockQuestionsToSaveLocal(page.questionGroups)
  };

  return {
    ...page,
    type: 'survey',
    page: {
      ...page.page,
      path: {
        key: page.pathKey
      },
      page: {
        key: page.pathPage
      }
    },
    answers: []
  }
});

export default prepareMockSurveyPages;
