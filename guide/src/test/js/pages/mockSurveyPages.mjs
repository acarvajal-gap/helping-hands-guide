import prepareMockSurveyPages from '../utils/prepareMockSurveyPages';

const entryPath = [
  {
    key: 'reasonForVisit',
    questionGroups: [
      {
        'key': 'reasonForVisitQuestionGroup',
        'type': 'Base',
        'questions': [
          {
            'appkey': 'reasonForVisit',
            'type': 'CheckboxIcon',
            'validation': [
              'required'
            ],
            'attributes': {
              'cssIcons': true
            },
            'dependsOn': [],
            'answers': [
              {
                'key': 'mortgage',
              },
              {
                'key': 'rent'
              },
              {
                'key': 'expenses'
              },
              {
                'key': 'utilities'
              },
              {
                'key': 'loans'
              },
              {
                'key': 'otherLoans'
              },
              {
                'key': 'creditCard'
              },
              {
                'key': 'tuition'
              },
              {
                'key': 'otherObligations'
              },
              {
                'key': 'retirementImpact'
              }
            ]
          }
        ]
      }
    ]
  },
  {
    key: 'affected',
    questionGroups: [
      {
        'key': 'affectedQuestionGroup',
        'type': 'Base',
        'questions': [
          {
            'appkey': 'affected.mostAffected',
            'type': 'Dropdown',
            'validation': [
              'required'
            ],
            'answers': [
              {
                'key': 'jobLoss'
              },
              {
                'key': 'laidOff'
              },
              {
                'key': 'furloughed'
              },
              {
                'key': 'payCut'
              },
              {
                'key': 'hoursReduced'
              },
              {
                'key': 'selfEmployed'
              },
              {
                'key': 'workingFromHome'
              }
            ]
          },
          {
            'appkey': 'affected.totalMonthlyIncome',
            'type': 'Slider',
            'validation': [
              'required'
            ],
            'dependsOn': [],
            'answers': [
              {
                'key': 0
              },
              {
                'key': 1000
              },
              {
                'key': 2000
              },
              {
                'key': 3000
              },
              {
                'key': 4000
              },
              {
                'key': 5000
              },
              {
                'key': 6000
              }
            ]
          },
          {
            'appkey': 'affected.totalReduceMonthlyIncome',
            'type': 'Slider',
            'validation': [
              'required'
            ],
            'dependsOn': [],
            'answers': [
              {
                'key': 0
              },
              {
                'key': 1000
              },
              {
                'key': 2000
              },
              {
                'key': 3000
              },
              {
                'key': 4000
              },
              {
                'key': 5000
              },
              {
                'key': 6000
              }
            ]
          },
        ]
      }
    ]
  },
  {
    key: 'lifeStage',
    questionGroups: [
      {
        'key': 'lifeStageQuestionGroup',
        'type': 'Base',
        'questions': [
          {
            'appkey': 'lifeStage',
            'type': 'Radio',
            'validation': [
              'required'
            ],
            'dependsOn': [],
            'answers': [
              {
                'key': true
              },
              {
                'key': false
              }
            ]
          },
          {
            'appkey': 'vehicleGoalAccess',
            'type': 'Checkbox',
            'validation': [],
            'dependsOn': [],
            'attributes': {},
            'answers': [
              {
                'key': 'no'
              },
              {
                'key': 'running'
              },
              {
                'key': 'may'
              },
              {
                'key': 'enough'
              },
              {
                'key': 'retirement'
              }
            ]
          },
          {
            'appkey': 'vehicleGoalWantsToBorrow',
            'type': 'Radio',
            'validation': [],
            'dependsOn': [],
            'answers': [
              {
                'key': true
              },
              {
                'key': false
              }
            ]
          }
        ]
      },
    ]
  },
  {
    key: 'lifeSituations',
    questionGroups: [
      {
        'key': 'lifeSituationsQuestionGroup',
        'type': 'Base',
        'questions': [
          {
            'appkey': 'lifeSituations',
            'type': 'CheckboxIcon',
            'attributes': {
              'cssIcons': true
            },
            'validation': [
              'required'
            ],
            'dependsOn': [],
            'answers': [
              {
                'key': 'incomeSources'
              },
              {
                'key': 'paymentObligations'
              },
              {
                'key': 'budget'
              },
              {
                'key': 'retirementFunds'
              },
              {
                'key': 'financialAdvisor'
              }
            ]
          }
        ]
      }
    ]
  },
  {
    key: 'goalTypes',
    questionGroups: [
      {
        'key': 'goalsQuestionGroup',
        'type': 'Base',
        'questions': [
          {
            'appkey': 'goalTypes',
            'type': 'Dropdown',
            'validation': [],
            'attributes': {},
            'dependsOn': [],
            'answers': [
              {
                key: 18,
              },
              {
                key: 24,
              },
              {
                key: 34,
              },
              {
                key: 44,
              },
              {
                key: 54,
              },
              {
                key: 64,
              },
              {
                key: 74,
              },
              {
                key: 75,
              }
            ]
          },
          {
            'appkey': 'vehicleGoalAmount',
            'type': 'Dropdown',
            'validation': [],
            'dependsOn': [],
            'answers': [
              {
                key: 'restaurant',
              },
              {
                key: 'health',
              },
              {
                key: 'freelance',
              },
              {
                key: 'farming',
              },
              {
                key: 'domestic',
              },
              {
                key: 'beauty',
              },
              {
                key: 'art',
              },
              {
                key: 'other',
              }
            ]
          },
          {
            'appkey': 'city',
            'type': 'TextInput',
            'attributes': {
              'dataType': 'text',
              'placeholder': false
            },
          },
          {
            'appkey': 'state',
            'type': 'TextInput',
            'attributes': {
              'dataType': 'text'
            },
          },
          {
            'appkey': 'zip',
            'type': 'TextInput',
            'attributes': {
              'dataType': 'text'
            },
          },
          {
            'appkey': 'vehicleGoalTiming',
            'type': 'Dropdown',
            'validation': [],
            'dependsOn': [],
            'attributes': {},
            'answers': [
              {
                'key': 'active'
              },
              {
                'key': 'veteran'
              },
              {
                'key': 'not'
              }
            ]
          }
        ]
      }
    ]
  }
];

const mockSurveyPages = [
  ...prepareMockSurveyPages(entryPath, 'entry')
];

export default mockSurveyPages;
