import React from 'react';
import classnames from 'classnames';
import {FormattedMessage} from 'react-intl';
import ReactSVG from 'react-svg';

const Header = ({className}) => {
  const classes = classnames(
    'header',
    className
  );

  return <>
    <header className={classes}>
      <div className="container">
        <div className="header__logo">
          <div><ReactSVG src="/classpath/assets/images/ignite.svg" /></div>
        </div>
      </div>
    </header>
  </>;
};

export default Header;
