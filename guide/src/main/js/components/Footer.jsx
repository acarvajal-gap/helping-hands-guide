import React from 'react';
import { FormattedHTMLMessage } from 'react-intl';

const Footer = () => {
  return <footer className="footer">
    <FormattedHTMLMessage id='footer.copyright' tag='span'/>
  </footer>;
};

export default Footer;
