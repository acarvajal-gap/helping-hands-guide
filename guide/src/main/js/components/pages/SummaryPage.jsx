import React from "react";
import classnames from "classnames";
import { connect } from "react-redux";
import {
  loadProductCart,
  updateProductCart,
} from "@ignitesales/neuro-application/src/actions/product-action-creators";
import buildPageUrl from "@ignitesales/neuro-application/src/page/buildPageUrl";
import { actionCompleted } from "@ignitesales/neuro-application/src/actions/question-action-creators";
import ProductGroup from "@ignitesales/neuro-application/src/components/products/ProductGroup";
import Product from "../Product";
import { FormattedMessage } from "react-intl";
import { goToNextPage } from "@ignitesales/neuro-application/src/actions/navigation-action-creators";
import ProgressBar from "../../components/ProgressBar";
import surveyPages from "../../../../test/js/pages/mockSurveyPages"; //'../pages/mockSurveyPages';
import ReactSVG from "react-svg";

function SummaryPage(props) {
  const {
    classname,
    products,
    actionCompleted,
    previousPageClicked,
    progress,
  } = props;

  const pageCssBEM = "summaryPage";
  const classes = classnames(pageCssBEM, classname);

  const renderProductGroup = (productGroup, subGroups, products) => {
    const cssBEM = "product-group";

    return (
      <div className={cssBEM} key={productGroup.name}>
        <div className={`${cssBEM}__header count-${products.length || 0}`}>
        </div>
        {subGroups || null}
        {products || null}
      </div>
    );
  };

  const renderProduct = (product) => {
    return (
      <Product
        product={product}
        key={product.id}
        actionCompleted={actionCompleted}
      />
    );
  };

  return (
    <div className={classes}>
      <header>
        <div className="container">
          <div className="header__logo">
            <div>
              <ReactSVG src="/classpath/assets/images/ignite.svg" />
            </div>
          </div>
          <ProgressBar progress={progress} />
        </div>
      </header>

      <div className={`${pageCssBEM}__header`}>
        <FormattedMessage id="summary.title" />
        <div className="toolbar">
          <button>
            <i className="fa fa-envelope-o"></i>
            <div>Email</div>
          </button>
          <button>
            <i className="fa fa-print"></i>
            <div>Print</div>
          </button>
          <button>
            <i className="fa fa-mobile-phone"></i>
            <div>Text</div>
          </button>
          <button>
            <i className="fa fa-user-circle-o"></i>
            <div>Contact</div>
          </button>
        </div>
      </div>
      <div className={`${pageCssBEM}__icon`}></div>
      <div className={`${pageCssBEM}__body`}>
        <div className={`${pageCssBEM}__subtitle`}>
          <FormattedMessage id="summary.subtitle" />
        </div>
        <div className={`${pageCssBEM}__products`}>
          {(products.groups || []).map((g) => (
            <ProductGroup
              group={g}
              renderGroupFn={renderProductGroup}
              renderProductFn={renderProduct}
              key={g.name}
            />
          ))}
        </div>
      </div>
      <hr />
      <button onClick={previousPageClicked} className="btn btn-secondary">
        <FormattedMessage id="nav.prev" />
      </button>
    </div>
  );
}

export default connect(
  (state) => {
    const totalPages = surveyPages.length || 1;
    const progress = {
      total: 100,
      totalPages,
      currentPage: totalPages,
    };
    const answers = state.answers || {};
    const products = state.products || {};
    const productCart = state.productCart;
    const user = state.user || {};
    const detailsUrl = buildPageUrl({
      type: "details",
      uuid: user.uuid,
    });
    return {
      answers,
      products,
      productCart: Array.isArray(productCart)
        ? new Set(productCart)
        : productCart,
      user,
      detailsUrl,
      progress,
    };
  },
  (dispatch) => {
    return {
      loadProducts: () => {
        dispatch(loadProductCart());
      },
      addToCartClicked: (products, productId) => {
        dispatch(updateProductCart(products, productId));
      },
      removeFromCartClicked: (products, productId) => {
        dispatch(updateProductCart(products, productId, true));
      },
      actionCompleted: (actionName, productId, value, append) => {
        dispatch(actionCompleted({ actionName, productId, value, append }));
      },
      previousPageClicked: () => {
        dispatch(goToNextPage(true));
      },
    };
  }
)(SummaryPage);
