import React from 'react';
import classnames from 'classnames';
import {connect} from 'react-redux';
import {loadProductCart} from '@ignitesales/neuro-application/src/actions/product-action-creators';
import {actionCompleted} from '@ignitesales/neuro-application/src/actions/question-action-creators';

const DetailsPage = ({classname}) => {

  const pageCssBEM = 'detailsPage';
  const classes = classnames(pageCssBEM, classname);

  return (
    <div className={classes} />
  );
};

export default connect((state) => {
  const answers = state.answers || {};
  const products = state.products || {};
  const user = state.user || {};
  return {
    user,
    answers,
    products
  };
}, dispatch => {
  return {
    loadProducts: () => {
      dispatch(loadProductCart());
    },
    actionCompleted: (actionName) => {
      dispatch(actionCompleted({actionName}));
    },
  };
})(DetailsPage);

