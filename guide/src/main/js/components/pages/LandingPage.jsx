import React from 'react';
import classnames from 'classnames';
import { actions } from '@ignitesales/neuro-application';
import {connect} from 'react-redux';
import {FormattedMessage} from 'react-intl';

const { startGuide } = actions.navigation;

const LandingPage = ({className, location, startGuideClicked, processingNavigation}) => {
  const bemClass = 'landingPage';
  const classes = classnames(bemClass, className);

  const startGuide = () => {
    startGuideClicked('checking', location);
  };

  return (
    <div className={classes}>
      <div className="tip"/>
      <div className="body">
        <div className={`${bemClass}__content`}>
          <div className="logo"/>
          <h1><FormattedMessage id="guide.name"/></h1>
          <h4><FormattedMessage id="landing.title"/></h4>
          <button onClick={startGuide} disabled={processingNavigation} className="btn btn-primary">
            <FormattedMessage id="landing.getStarted"/>
            <i className="fa fa-caret-right" />
          </button>
        </div>
      </div>
    </div>
  );
};

export {
  LandingPage as DisconnectedLandingPage
};

export default connect(
  ({processingNavigation = false}) => {
    return {
      processingNavigation
    };
  },
  dispatch => {
    return {
      startGuideClicked: selectedPath => dispatch(startGuide(selectedPath))
    };
  }
)(LandingPage);
