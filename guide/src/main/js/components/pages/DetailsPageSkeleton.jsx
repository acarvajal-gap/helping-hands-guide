import React from 'react';
import classnames from 'classnames';

const DetailsPageSkeleton = () => {
  const pageCssBEM = 'detailsPage';
  const classes = classnames( pageCssBEM, {
    [`${pageCssBEM}--skeleton`]: true
  });

  return (
    <div className={classes}/>
  );
};

export default DetailsPageSkeleton;

