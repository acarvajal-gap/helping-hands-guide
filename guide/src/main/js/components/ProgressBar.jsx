import React from 'react';
import classnames from 'classnames';
import { FormattedMessage } from 'react-intl';

const ProgressBar = ({className, progress}) => {
  const classes = classnames('progressbar-container', className);
  const { currentPage, totalPages } = progress;

  const status = (index) => {
    if (index < currentPage) {
      return 'is-complete';
    }
    if (index === currentPage) {
      return 'is-active';
    }
    return '';
  };

  const listItems = [...Array(totalPages).keys()].map((page) => {
    const pageNumber = page + 1;
    return (
      <li className={`step ${status(pageNumber)}`} data-step={pageNumber} key={pageNumber}>
        {pageNumber === 1 &&
          <FormattedMessage id={'progress.title'} tagName="span"/>
        }
      </li>
    );
  });

  return <div className={classes}>
    <ul className="steps">
      {listItems}
    </ul>
  </div>;
};

export default ProgressBar;
