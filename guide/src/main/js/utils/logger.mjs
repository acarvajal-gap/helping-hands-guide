import { createLogger } from '@ignitesales/logger-es';

/* eslint-disable-next-line no-undef */
const logger = createLogger(LOGGER_CONFIG);

// register error and unhandledrejection listeners for window
if (window) {
  logger.registerGlobalHandlers();
}

export default logger;
