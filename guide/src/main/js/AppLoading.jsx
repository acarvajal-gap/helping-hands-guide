import React from 'react';
import {Route} from 'react-router-dom';
import {DisconnectedLandingPage} from './components/pages/LandingPage';
import SurveyPageSkeleton from './components/pages/SurveyPageSkeleton';
import SummaryPageSkeleton from './components/pages/SummaryPageSkeleton';
import DetailsPageSkeleton from './components/pages/DetailsPageSkeleton';
import Header from './components/Header';

const AppLoading = () => {

  return <div className="app app-content-container app--loading">
    <Header />
    <div className="pageContainer">
      <Route exact={true} path="/guide" render={() => (
        <DisconnectedLandingPage className="loading" />
      )}/>
      <Route path="/guide/spa/:uuid/:path/:page" component={SurveyPageSkeleton}/>
      <Route path="/guide/:uuid/summary" component={SummaryPageSkeleton}/>
      <Route path="/guide/:uuid/details" component={DetailsPageSkeleton}/>
    </div>
  </div>;
};

export default AppLoading;
