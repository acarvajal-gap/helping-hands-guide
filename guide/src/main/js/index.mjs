import logger from './utils/logger.mjs';
import App from './App';
import { initializeNeuroApplication } from '@ignitesales/neuro-application';
import AppLoading from './AppLoading';
import messages from './i18n/messages';
import WebFont from 'webfontloader';

const init = async () => {
  WebFont.load({
    google: {
      families: [ 'Source Sans Pro:200,300,300i,400,400i,600,600i,700,700i,900' ]
    }
  });

  await initializeNeuroApplication(
    {
      AppComponent: App,
      LoadingComponent: AppLoading,
      messages,
      isBranchGuide: false,
      supportedLocales: ['en'],
      reducers: []
    }
  );

  logger.info('Go for takeoff!');
};

init();
