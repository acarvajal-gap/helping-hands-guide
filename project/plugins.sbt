addSbtPlugin("com.ignitesales.sbt.plugins" % "ignite-guide-plugin" % "3.3.1")

resolvers += Resolver.url("Ignite SBT Plugin Repository", url("https://artifactory.ignitesales.com/artifactory/sbt-plugins"))(Resolver.ivyStylePatterns)